
DESTDIR=/
BINDIR=$(DESTDIR)/usr/bin
SYSCONFDIR=$(DESTDIR)/etc
OPTDIR=$(DESTDIR)/opt
DATADIR=$(DESTDIR)/usr/share

.PHONY: all run clean edit

run:
	./src/pillar.py -v


clean:
	-rm -r src/pillar/__pycache__

edit:
	gedit -s src/pillar.py src/pillar/operator.py src/pillar/events.py src/pillar/model.py src/pillar/view.py 2> /dev/null &



