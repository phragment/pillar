# This file is part of Pillar
# Copyright 2023 Thomas Krug
#
# SeamLess is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SeamLess is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import datetime
import json
import mimetypes
import os
import time
import threading

import gi
from gi.repository import Gio

from pillar import events


class Model:

    def __init__(self, log, settings, view):

        self.log = log.getChild(type(self).__name__)
        self.settings = settings
        self.view = view

        self.monitor = Monitor(self)

        self.column_id_last = -1
        self.column_ids = []
        self.columns = {}

    def add_column(self, info):
        #cid = self.column_id_last + 1
        import uuid
        cid = uuid.uuid4()

        self.columns[cid] = info
        self.column_ids.append(cid)

        self.log.debug("added column: %s %s", cid, info)

        #self.column_id_last = cid
        return cid

    def remove_column(self, cid):
        self.column_ids.remove(cid)
        self.columns.remove[cid]

    def get_path(self, cid):
        column = self.columns.get(cid, None)
        dp = column["dp"]
        return dp

    def get_cid(self, path):
        #print(self.columns)

        for cid, column in self.columns.items():
            #print("get cid", cid, column)
            if column["dp"] == path:
                return cid
        return None

    def open_dir(self, dp, parent):

        """
        ## remove existing column on same level
        for cid in reversed(self.column_ids):
            parent = os.path.dirname(dp)
            if os.path.dirname(cid).startswith(parent):
                # this check will probably not work with bookmarks, so save "parent column"
                # and close all columns with this parent
                self.log.debug("remove column %s", cid)

                self.view.remove_column(cid)
                self.column_ids.remove(cid)
                del self.columns[cid]
        """

        if parent:
            print(parent, self.column_ids)
            # close all columns to the right of parent
            pos_parent = self.column_ids.index(parent)
            print(pos_parent, self.column_ids[pos_parent+1:])
            for cid in self.column_ids[pos_parent+1:]:
                self.view.remove_column(cid)
                self.column_ids.remove(cid)
                del self.columns[cid]

        if not os.path.isdir(dp):
            self.log.warning("not a directory: %s", dp)
            # TODO handle

            # sanitize
            # go one up until path is a dir or empty?

        ## setup column
        dp = os.path.abspath(dp)  # this changes relative path to cwd
        cid = self.add_column({"dp": dp})

        label = os.path.basename(dp)
        self.view.add_column(cid, {"label": label})

        # monitor
        self.monitor.add(dp)

        ##
        thread = threading.Thread(
            name="scandir",
            args=[cid, dp],
            target=self.scan_dir
        )
        thread.start()


    # FIXME move part of this to add_file method
    def scan_dir(self, cid, dp):
        res = {"files": [], "dirs": []}
        for de in os.scandir(dp):
            fn = de.name
            try:
                tm = de.stat().st_mtime  # content modification
                dt = datetime.datetime.fromtimestamp(tm)
                mod = dt.strftime("%Y-%m-%d %H:%M")
            except FileNotFoundError:
                mod = ""

            # TODO move to data class (explicit interface!)
            info = {"dir": dp, "name": fn, "mod": mod}

            if de.is_dir():
                info["type"] = "dir"
            else:
                info["type"] = "file"

            # TODO
            #type, encoding = mimetypes.guess_type(uri)

            if de.is_dir():
                info["icon-name"] = "folder"
                #info["icon"] = Gio.ThemedIcon.new_with_default_fallbacks("folder")
            else:
                mime_type, enc = mimetypes.guess_type(fn)
                if mime_type:
                    typ = Gio.content_type_from_mime_type(mime_type)
                    icon_name = Gio.content_type_get_generic_icon_name(typ)
                    info["icon-name"] = icon_name
                    #print(fn, mime_type, typ, icon_name)
                    #info["icon"] = Gio.ThemedIcon.new_with_default_fallbacks(icon_name)

            self.view.add_entry(cid, fn, info)

            # TODO
            # describe info format
            # FIXME use data class?

            # FIXME its probably better to use abspath plus label here
            # then label can be used as "nickname" in shortcut column

    def add_file(self, fp):
        dp, fn = os.path.split(fp)

        cid = self.get_cid(dp)
        if not cid:
            return

        status = os.stat(fp)
        dt = datetime.datetime.fromtimestamp(status.st_mtime)
        mod = dt.strftime("%Y-%m-%d %H:%M")

        info = {"dir": dp, "name": fn, "mod": mod}

        if os.path.isdir(fp):
            info["type"] = "dir"
        else:
            info["type"] = "file"

        if info["type"] == "dir":
            info["icon-name"] = "folder"
            #info["icon"] = Gio.ThemedIcon.new_with_default_fallbacks("folder")
        else:
            mime_type, enc = mimetypes.guess_type(fn)
            if mime_type:
                typ = Gio.content_type_from_mime_type(mime_type)
                icon_name = Gio.content_type_get_generic_icon_name(typ)
                info["icon-name"] = icon_name
                #print(fn, mime_type, typ, icon_name)
                #info["icon"] = Gio.ThemedIcon.new_with_default_fallbacks(icon_name)

        self.view.add_entry(cid, fn, info)

    def remove_file(self, fp):
        dp, fn = os.path.split(fp)

        # only abspath
        if not dp:
            return

        cid = self.get_cid(dp)
        if not cid:
            return

        self.view.remove_entry(cid, fn)

    def open_shortcuts(self):
        shortcuts = []

        shortcuts.append("/")  # root
        shortcuts.append("~")  # home

        # load bookmarks
        # TODO

        # TODO
        cid = self.add_column({"dp": None})

        self.view.add_column(cid, {"label": "shortcuts"})

        for shortcut in shortcuts:
            info = {"dir": "", "name": shortcut, "mod": "", "type": "dir"}
            self.view.add_entry(cid, shortcut, info)




class Monitor:

    def __init__(self, model):
        self.model = model
        self.log = model.log.getChild(type(self).__name__)

        self.monitors = {}

    def add(self, dp):
        self.log.debug("adding file monitor %s", dp)
        gf = Gio.File.new_for_path(dp)
        monitor = gf.monitor_directory(Gio.FileMonitorFlags.WATCH_MOVES)
        monitor.connect("changed", self.on_changed)
        self.monitors[dp] = monitor

    def remove(self, dp):
        self.log.debug("removing file monitor %s", dp)
        fm = self.monitors[dp]
        fm.cancel()
        self.monitors[dp] = None

    def on_changed(self, file_monitor, f, of, event_type):
        fp = f.get_path()
        if of:
            fp_new = of.get_path()
        self.log.debug("file event %s %s", event_type, fp)

        if event_type == Gio.FileMonitorEvent.CREATED:
            self.log.debug("created, add")
            self.model.add_file(fp)

        if event_type == Gio.FileMonitorEvent.MOVED_IN:
            self.log.debug("moved in, add")
            self.model.add_file(fp)

        if event_type == Gio.FileMonitorEvent.DELETED:
            self.log.debug("deleted, remove")
            self.model.remove_file(fp)

        if event_type == Gio.FileMonitorEvent.MOVED_OUT:
            self.log.debug("moved out, remove")
            self.model.remove_file(fp)

        if event_type == Gio.FileMonitorEvent.RENAMED:
            self.log.debug("renamed, remove old, add new")
            self.model.remove_file(fp)
            self.model.add_file(fp_new)


