# This file is part of Pillar
# Copyright 2023 Thomas Krug
#
# SeamLess is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SeamLess is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import os
import urllib

import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, GLib, Gdk, GObject, Pango, Gio

from pillar import events


# decorator for enqueuing functions into gtk main thread
def gtk(func):
    def wrapper(*args, **kwargs):
        #print("view wrapper, called by", threading.current_thread().name)
        GLib.idle_add(func, *args, **kwargs)
    return wrapper


class View:

    def __init__(self, log, settings, operator):
        self.log = log.getChild(type(self).__name__)
        self.settings = settings
        self.operator = operator

    def start(self):
        self.log.debug("starting")
        self.app = Application(self.log, self)
        self.app.run()

    @gtk
    def stop(self):
        self.log.debug("stopping")
        self.app.win.win.destroy()

    @gtk
    def add_column(self, ident, info):
        self.app.win.columns.add_column(ident, info)

    @gtk
    def remove_column(self, ident):
        self.app.win.columns.remove_column(ident)

    @gtk
    def add_entry(self, colid, eid, info):
        self.app.win.columns.add_entry(colid, eid, info)

    @gtk
    def remove_entry(self, colid, eid):
        self.app.win.columns.remove_entry(colid, eid)

    @gtk
    def focus_column(self, ident=None):
        self.log.debug("focus column")
        #self.app.win.columns.columns[]
        # TODO


class Application:

    def __init__(self, log, view):
        self.log = log.getChild(type(self).__name__)
        self.view = view
        self.app = Gtk.Application.new(None, 0)
        self.app.connect("activate", self.activate)

    def run(self):
        self.log.debug("run")
        self.app.run()

    def activate(self, app):
        self.log.debug("activate")
        self.win = MainWindow(self.log, self.view, app)


class MainWindow:

    def __init__(self, log, view, app):
        self.log = log.getChild(type(self).__name__)
        self.view = view

        self.win = Gtk.ApplicationWindow.new(app)

        self.win.set_title("Pillar")
        #self.win.set_icon_name("")

        self.win.connect("close-request", self.on_window_close_request)

        self.win.set_default_size(940, 480)

        self.vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        self.win.set_child(self.vbox)

        self.columns = Columns(self.log, self.view)
        self.vbox.append(self.columns.get_widget())

        self.sidepane_new = SidepaneNew(self.log, self.view)
        #self.hbox.append(self.sidepane_new.get_widget())

        self.inputbar = InputBar(self.log, self.view)
        self.vbox.append(self.sidepane_new.get_widget())

        """
        TODO
        hide window header
        set as dialog?
        """

        #self.win.set_titlebar(Gtk.Box())


        ec_key = Gtk.EventControllerKey()
        ec_key.connect("key-released", self.on_key_released)
        self.win.add_controller(ec_key)

        self.log.debug("init done, show window")
        self.win.show()

    def on_window_close_request(self, window):
        self.view.operator.enqueue(events.Quit())
        # return event handled, otherwise window shuts down immediately
        return True

    def on_key_released(self, ec, keyval, keycode, state):
        pass
        #if keyval == Gdk.KEY_Escape:
        #    self.view.operator.enqueue(events.Quit())



class Columns:

    def __init__(self, log, view):
        self.log = log.getChild(type(self).__name__)
        self.view = view

        self.sw = Gtk.ScrolledWindow()
        self.sw.props.vscrollbar_policy = Gtk.PolicyType.NEVER

        self.sw.props.hexpand = True

        # TODO shift scroll for left/right
        self.box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 2)
        self.sw.set_child(self.box)

        self.column_ids = []
        self.columns = {}

        self.column_cur = -1

        ec_key = Gtk.EventControllerKey()
        ec_key.connect("key-released", self.on_key_released)
        self.box.add_controller(ec_key)

    def get_widget(self):
        return self.sw

    def add_column(self, ident, info):
        self.log.debug("add column: %s", ident)

        column = Column(self.log, self.view, ident, info)

        self.columns[ident] = column
        self.box.append(column.get_widget())
        self.column_ids.append(ident)
        self.column_cur += 1

        column.list.grab_focus()

    def remove_column(self, ident):
        column = self.columns[ident]
        self.column_ids.remove(ident)
        self.box.remove(column.get_widget())
        del self.columns[ident]
        self.column_cur -= 1
        # needed?
        #self.columns[self.column_ids[self.column_cur]].list.grab_focus()

    # FIXME do batch updates?
    def add_entry(self, ident, eid, info):
        column = self.columns.get(ident, None)
        if not column:
            self.log.debug("no column found: %s", ident)
            return

        column.add_entry(eid, info)

    def remove_entry(self, ident, eid):
        column = self.columns.get(ident, None)
        if not column:
            self.log.debug("no column found: %s", ident)
            return

        column.remove_entry(eid)

    def on_key_released(self, ec, keyval, keycode, state):

        if keyval == Gdk.KEY_Right:
            self.log.debug("focus right column")
            self.column_cur = (self.column_cur + 1) % len(self.column_ids)
            self.columns[self.column_ids[self.column_cur]].list.grab_focus()
            return True

        if keyval == Gdk.KEY_Left:
            self.log.debug("focus left column")
            self.column_cur = (self.column_cur - 1) % len(self.column_ids)
            self.columns[self.column_ids[self.column_cur]].list.grab_focus()
            return True


class Entry(GObject.Object):

    def __init__(self, ident, text):
        super().__init__()
        self.ident = ident
        self.text = text
        self._ctrl = None

    def set_string(self, text):
        self.text = text

    def get_string(self):
        return self.text


class Column:

    def __init__(self, log, view, ident, info):
        self.log = log.getChild(type(self).__name__)
        self.view = view

        self.ident = ident
        self.info = info

        # TODO add column header
        self.box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 2)
        self.label = Gtk.Label.new(self.info["label"])
        self.box.append(self.label)

        self.sw = Gtk.ScrolledWindow()
        self.sw.props.hscrollbar_policy = Gtk.PolicyType.NEVER

        # set minimal size, allow expanding
        self.sw.props.hexpand = True
        self.sw.props.vexpand = True
        self.sw.set_size_request(300, -1)

        if info["label"] == "shortcuts":
            self.sw.props.hexpand = False
            self.sw.props.width_request = 200

        self.box.append(self.sw)

        ## ListView
        self.list = Gtk.ListView()
        self.sw.set_child(self.list)

        self.factory = Gtk.SignalListItemFactory()
        self.factory.connect("setup", self.on_factory_setup)
        self.factory.connect("bind", self.on_factory_bind)
        self.list.set_factory(self.factory)

        self.selection = Gtk.MultiSelection()
        self.model = Gio.ListStore()
        self.selection.set_model(self.model)
        self.list.set_model(self.selection)

        self.list.connect("activate", self.on_row_activated)

        self.menu = RightClickMenu(self)

        ## dnd
        actions = Gdk.DragAction.ASK
        actions |= Gdk.DragAction.COPY
        actions |= Gdk.DragAction.MOVE

        # drag
        self.ctrl_drag = Gtk.DragSource()
        self.ctrl_drag.set_actions(actions)
        self.ctrl_drag.connect("prepare", self.on_drag_prepare)
        self.list.add_controller(self.ctrl_drag)

        # drop
        # TODO has ask to be implemented in handler?
        self.ctrl_drop = Gtk.DropTarget()
        self.ctrl_drop.set_actions(actions)
        #self.ctrl_drop = Gtk.DropTarget.new(
        #    type=GObject.TYPE_NONE,
        #    actions=actions
        #)
        self.ctrl_drop.set_gtypes( [Gdk.FileList, str] )
        self.ctrl_drop.connect("drop", self.on_drop)
        self.box.add_controller(self.ctrl_drop)

        ## cnp
        self.clipman = ClipboardManager()

        ec_key = Gtk.EventControllerKey()
        ec_key.connect("key-released", self.on_key_released)
        #wdgt = self.view.app.win.win
        wdgt = self.list
        wdgt.add_controller(ec_key)


    def get_widget(self):
        return self.box

    def add_entry(self, ident, info):
        #self.log.debug("add entry %s", info)

        # hidden files
        name = info["name"]
        if name.startswith("."):
            return

        # TODO sort when inserting?
        text = info["name"]

        entry = Entry(ident, text)
        entry.info = info  # FIXME ?
        #self.model.append(entry)
        self.model.insert_sorted(entry, self.compare)

    def remove_entry(self, ident):
        # FIXME
        #entry = Entry(ident, "")
        #found, pos = self.model.find_with_equal_func(entry, self._is_equal)
        #if found:
        #    self.model.remove(pos)

        for pos in range(self.model.get_n_items()):
            entry = self.model.get_item(pos)
            if not entry:
                continue
            if entry.ident == ident:
                self.model.remove(pos)


    # TODO move to Entry class?
    #def _is_equal(self, a, b):
    #    print(self, a, b)
    #    return False

    def compare(self, a, b):
        #print(a, b)

        if a.info["type"] == "dir" and b.info["type"] == "file":
            return -1
        if b.info["type"] == "dir" and a.info["type"] == "file":
            return 1

        if a.text.lower() < b.text.lower():
            return -1
        if a.text.lower() > b.text.lower():
            return 1

        # a < b  -1
        # a = b  0
        # a > b  1
        return 0

    def on_row_activated(self, list_view, position):
        self.log.debug("pos activated %s", position)

        # TODO get selection (multi)

        item = self.model.get_item(position)
        ident = item.ident

        self.log.debug("open %s", ident)

        # TODO decide what to in event?
        info = item.info
        if info["type"] == "dir":
            dp = os.path.join(info["dir"], info["name"])
            self.view.operator.enqueue(events.OpenDir(dp, self.ident))
        else:
            fp = os.path.join(info["dir"], info["name"])
            self.view.operator.enqueue(events.OpenFile(fp))

    def on_factory_setup(self, factory, listitem):
        box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 2)

        img = Gtk.Image()
        box.append(img)

        label = Gtk.Label()
        label.props.xalign = 0
        label.props.wrap = True
        label.props.wrap_mode = Pango.WrapMode.WORD_CHAR
        label.props.max_width_chars = 50
        #label.props.width_chars = 20
        box.append(label)

        box.img = img
        box.label = label

        listitem.set_child(box)

    def on_factory_bind(self, factory, listitem):
        box = listitem.get_child()
        entry = listitem.get_item()

        text = entry.get_string()
        label = box.label
        label.set_label(text)

        img = box.img
        icon_name = entry.info.get("icon-name", None)
        if icon_name:
            img.set_from_icon_name(icon_name)

    def get_selected(self):
        # multiselection
        #bitset = self.selection.get_selected()

        idents = []
        num = self.selection.get_n_items()
        for pos in range(num):
            if self.selection.is_selected(pos):
                item = self.selection.get_item(pos)
                idents.append(item.ident)

        # get dir path
        model = self.view.operator.model
        dp = model.get_path(self.ident)

        filepaths = []
        for fn in idents:
            filepaths.append( os.path.join(dp, fn) )

        return filepaths

    def on_drag_prepare(self, ctrl, x, y, test=""):
        self.log.debug("drag prepare: %s", test)

        paths = self.get_selected()
        gfiles = []
        for fp in paths:
            gfile = Gio.File.new_for_path(fp)
            gfiles.append(gfile)

        if not gfiles:
            self.log.debug("nothing selected")
            return None

        file_list = Gdk.FileList.new_from_list(gfiles)
        content_provider = Gdk.ContentProvider.new_for_value(file_list)
        return content_provider

    def on_drop(self, ctrl, value, x, y):
        self.log.debug("drop")
        #print(ctrl.props.current_drop)

        # TODO offer menu on dragaction.ask

        files = []
        if isinstance(value, Gdk.FileList):
            gfiles = value.get_files()

            for gfile in gfiles:
                files.append(gfile.get_path())

        if isinstance(value, str):
            files.append(value)

        #for f in files:
        #    print(f)

        model = self.view.operator.model
        dp = model.get_path(self.ident)

        self.view.operator.enqueue(events.Copy(files, dp))


    def on_key_released(self, ec, keyval, keycode, state):
        if state & Gdk.ModifierType.CONTROL_MASK:
            if keyval == Gdk.KEY_c:
                paths = self.get_selected()
                self.clipman.set_filelist(paths)
                return True

            if keyval == Gdk.KEY_x:
                paths = self.get_selected()
                self.clipman.set_filelist(paths, action="cut")
                return True

            if keyval == Gdk.KEY_v:
                self.clipman.get_filelist(self.got_filelist)
                return True

    def got_filelist(self, action, files):
        #self.log.debug()

        model = self.view.operator.model
        dp = model.get_path(self.ident)

        if action == "copy":
            event = events.Copy(files, dp)

        if action == "cut":
            event = events.Move(files, dp)

        self.view.operator.enqueue(event)


class ClipboardManager:

    def __init__(self):
        self.clipboard = Gdk.Display.get_default().get_clipboard()

    def set_text(self, text):
        self.clipboard.set(text)

    def get_text(self, callback):
        self.cb_text = callback
        self.clipboard.read_text_async(None, self._got_text)

    def _got_text(self, clipboard, result):
        try:
            text = clipboard.read_text_finish(result)
        except GLib.GError:
            return

        self.cb_text(text)

    def set_image(self, texture):
        print("TODO")
        return

    def get_image(self, callback):
        self.cb_image = callback
        self.clipboard.read_texture_async(None, self._got_image)

    def _got_image(self, clipboard, result):
        try:
            texture = clipboard.read_texture_finish(result)
        except GLib.GError:
            return

        self.cb_image(texture)

    def set_filelist(self, files, action="copy"):
        urls = [action]
        for f in files:
            url = "file://"
            url += urllib.parse.quote(os.path.abspath(f))
            urls.append(url)

        data = "\n".join(urls).encode()
        gbytes = GLib.Bytes.new(data)

        content = Gdk.ContentProvider.new_for_bytes("x-special/gnome-copied-files", gbytes)
        self.clipboard.set_content(content)


    def get_filelist(self, callback):
        self.cb_filelist = callback

        formats = self.clipboard.get_formats()
        mime_types = formats.get_mime_types()

        if "x-special/gnome-copied-files" in mime_types:
            self.expected = "gnome-copied-files"
            self.clipboard.read_async(
                ["x-special/gnome-copied-files"],
                GLib.PRIORITY_DEFAULT,
                None,
                self._got_filelist_stream
            )
            return

        if "text/uri-list" in mime_types:
            self.expected = "uri-list"
            self.clipboard.read_async(
                ["text/uri-list"],
                GLib.PRIORITY_DEFAULT,
                None,
                self._got_filelist_stream
            )
            return


    def _got_filelist_stream(self, clipboard, result):
        try:
            stream, mime = clipboard.read_finish(result)
        except GLib.GError:
            return

        #self.readsize = 10  # test
        self.readsize = 4096
        self.buffer = b""
        stream.read_bytes_async(self.readsize, GLib.PRIORITY_DEFAULT, None, self._get_filelist_data)

    def _get_filelist_data(self, stream, result):
        gbytes = stream.read_bytes_finish(result)
        data = gbytes.get_data()

        self.buffer += data

        if len(gbytes.get_data()) != self.readsize:
            self._got_filelist(self.buffer)
            return

        stream.read_bytes_async(self.readsize, GLib.PRIORITY_DEFAULT, None, self._get_filelist_data)

    def _got_filelist(self, data):
        data = data.decode()  # assume utf8
        data = data.split("\n")

        if self.expected == "gnome-copied-files":
            action = data[0]
            data = data[1:]
        else:
            action = "copy"

        files = []
        for url in data:
            #res = urllib.parse.urlparse(url)
            #fp = os.path.join(res.netloc, res.path)
            fp = urllib.parse.unquote(url[7:])
            files.append(fp)

        self.cb_filelist(action, files)


class MenuEntry(GObject.Object):

    def __init__(self, ident, text, func):
        super().__init__()
        self.ident = ident
        self.text = text
        self.func = func

class RightClickMenu:

    def __init__(self, parent):
        self.parent = parent
        self.log = parent.log
        self.view = parent.view
        self.ident = parent.ident

        parent_wdgt = parent.list

        self.popover = Gtk.Popover()
        self.popover.set_parent(parent_wdgt)
        self.popover.props.has_arrow = False

        self.listbox = Gtk.ListBox()
        self.popover.set_child(self.listbox)

        self.liststore = Gio.ListStore()
        self.listbox.bind_model(self.liststore, self.create_entry)

        entries = [
            ("create-folder", "Create Folder", self.menu_create_folder),
            ("create-file", "Create File", self.menu_create_file),
            ("rename", "Rename", self.menu_rename),
            ("copy", "Copy", self.menu_copy),
            ("paste", "Paste", self.menu_paste),
            ("cut", "Cut", self.menu_cut),
            ("delete", "Delete", self.menu_delete)
        ]
        for ident, text, func in entries:
            self.liststore.append(MenuEntry(ident, text, func))

        self.listbox.props.selection_mode = Gtk.SelectionMode.NONE
        self.listbox.props.activate_on_single_click = True
        self.listbox.connect("row-activated", self.on_row_activated)

        # add controller to parent
        ec_gc = Gtk.GestureClick()
        ec_gc.props.button = Gdk.BUTTON_SECONDARY
        ec_gc.connect("released", self.on_button_released, Gdk.BUTTON_SECONDARY)
        parent_wdgt.add_controller(ec_gc)


    def create_entry(self, item):
        #print(item)

        label = Gtk.Label()
        label.props.label = item.text
        label.props.xalign = 0

        return label

    def show(self, x, y):
        #self.popover.set_pointing_to( Gdk.Rectangle(x, y, 1, 1) )
        self.popover.set_pointing_to( Gdk.Rectangle(0, 0, 1, 1) )
        self.popover.set_offset(x, y)

        #self.popover.present()
        self.popover.popup()

    def hide(self):
        # TODO how to auto hide if user clicks outside of window (on unfocus?)
        # EventControllerFocus ? kb only?
        # on unfocus window?
        # window.is_active
        # https://gitlab.gnome.org/GNOME/gtk/-/issues/3502
        # x11 "bug" ?

        # escape hides by default
        self.popover.popdown()

    def on_button_released(self, ec, press, x, y, button):
        if button == Gdk.BUTTON_SECONDARY:
            self.log.debug("right click on %s", self.ident)
            self.show(x, y)

    def on_row_activated(self, listbox, row):
        self.log.debug("row act")

        pos = row.get_index()
        item = self.liststore.get_item(pos)

        item.func()

        self.hide()

    def menu_create_folder(self):
        self.log.debug("create folder")

        model = self.view.operator.model
        dp = model.get_path(self.parent.ident)

        print("TODO create dir in", dp)

    def menu_create_file(self):
        self.log.debug("create file")

        model = self.view.operator.model
        dp = model.get_path(self.parent.ident)

        print("TODO create file in", dp)

    def menu_rename(self):
        self.log.debug("rename file")

        paths = self.parent.get_selected()
        if not paths:
            return

        fp = paths[0]
        print("TODO rename", fp)

    def menu_copy(self):
        self.log.debug("copy files")
        paths = self.parent.get_selected()
        if not paths:
            return
        self.parent.clipman.set_filelist(paths)

    def menu_cut(self):
        self.log.debug("cut files")
        paths = self.parent.get_selected()
        if not paths:
            return
        self.parent.clipman.set_filelist(paths, action="cut")

    def menu_paste(self):
        self.log.debug("paste files")
        self.parent.clipman.get_filelist(self.parent.got_filelist)

    def menu_delete(self):
        self.log.debug("delete files")

        # TODO move to trash


# TODO copy pwd as entry

# TODO open menu on menu key





class SidepaneNew:

    def __init__(self, log, view):
        self.log = log
        self.view = view

        self.box = Gtk.Box()

    def get_widget(self):
        return self.box


class InputBar:

    def __init__(self, log, view):
        self.log = log
        self.view = view

        self.box = Gtk.Box()

    def get_widget(self):
        return self.box

















