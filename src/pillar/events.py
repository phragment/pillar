# This file is part of Pillar
# Copyright 2023 Thomas Krug
#
# SeamLess is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SeamLess is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import os
import subprocess
import shutil
import time


class Event:

    def process(self, operator):
        raise NotImplementedError("no process function")


class Quit(Event):

    def process(self, operator):
        operator.log.debug("shutting down")
        operator.view.stop()
        operator.stop()


class LoadApplications(Event):

    def process(self, operator):
        operator.log.debug("loading applications")
        operator.model.load_applications()


class LoadPresets(Event):

    def process(self, operator):
        operator.log.debug("loading presets")
        operator.model.load_presets()


class LoadShortcuts(Event):

    def process(self, operator):
        operator.log.debug("loading shortcuts")
        operator.model.open_shortcuts()


class FocusColumn(Event):

    def process(self, operator):
        operator.model.view.focus_column()


class OpenDir(Event):

    def __init__(self, dp, parent=""):
        dp = os.path.normpath(dp)
        dp = os.path.expanduser(dp)

        self.dp = dp
        self.parent = parent

    def process(self, operator):
        operator.log.debug("opening dir: %s, parent %s", self.dp, self.parent)
        operator.model.open_dir(self.dp, self.parent)


class OpenFile(Event):

    def __init__(self, fp):
        self.fp = fp

    def process(self, operator):
        operator.log.debug("opening file: %s", self.fp)

        # TODO write own
        # use file (incl magic numbers) here?
        # use guessed mimetype (file extension) for images?

        # gio content-type vs fast-content-type ? is content-type correct?

        cmd = ["xdg-open", self.fp]

        try:
          proc = subprocess.Popen(
              cmd,
              start_new_session=True,
              stdout=subprocess.DEVNULL,
              stderr=subprocess.DEVNULL
          )
        except FileNotFoundError:
            operator.log.error("command not found: %s", self.cmd)


class Copy(Event):

    def __init__(self, sources, dst):
        self.sources = sources
        self.dst = dst

    def process(self, operator):
        operator.log.debug("copy %s to %s", self.sources, self.dst)

        # also copy metadata
        for fp in self.sources:
            fn = os.path.basename(fp)
            dst_fp = os.path.join(self.dst, fn)
            #operator.log.debug("%s to %s", fp, dst_fp)
            shutil.copy2(fp, dst_fp)

# copy atomic
# 1. copy to hidden file next to target
# 2. check if file ok?
# 3. rename hidden file to target

# FIXME allow copying tree!

# shutil.copytree / rmtree
# does copytree really not overwrite?
#    dirs_exist_ok=True ???

# move atomic
# 1. copy (to hidden file next to target ?)
# 2. check if file ok
# 3. remove source file

# move -> copy + del

class Move(Event):

    def __init__(self, sources, target):
        self.sources = sources
        self.target = target

    def process(self, operator):
        operator.log.debug("move %s to %s", self.sources, self.target)

        for fp in self.sources:
            fn = os.path.basename(fp)
            dst_fp = os.path.join(self.dst, fn)
            #operator.log.debug("%s to %s", fp, dst_fp)
            shutil.move(fp, dst_fp)

#class Delete(Event):

# https://specifications.freedesktop.org/trash-spec/trashspec-1.0.html
#class MoveToTrash(Event):

    #Gio.File.trash()



class Rename(Event):

    def __init__(self, source, target):
        self.source = source
        self.target = target

    def process(self, operator):
        operator.log.debug("rename %s to %s", self.sources, self.target)

        os.rename(self.source, self.target)





















